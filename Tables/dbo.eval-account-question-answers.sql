CREATE TABLE [dbo].[eval-account-question-answers]
(
[user_id] [bigint] NULL,
[ email_alias] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ user_email] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ first_name] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ last_name] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ company_code] [varchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ question] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ answer] [bigint] NULL
) ON [PRIMARY]
GO
