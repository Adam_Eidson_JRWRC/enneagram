SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Adam Eidson
-- Create date: 11/26/2019
-- Description:	Stored procedure to parse out the Enneagram data for Goal Presentation
-- =============================================
CREATE PROCEDURE [dbo].[ParseGoalPresentation]
	-- Add the parameters for the stored procedure here
	@Name varchar(75) ,
	@StringVar varchar(5000)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
SELECT SUBSTRING(@StringVar,CHARINDEX('are:', @StringVar)+4,CHARINDEX(@Name,@StringVar)-(CHARINDEX('are:', @StringVar)+4))

END
GO
