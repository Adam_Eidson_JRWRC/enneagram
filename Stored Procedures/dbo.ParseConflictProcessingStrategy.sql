SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Adam Eidson
-- Create date: 11/26/2019
-- Description:	Stored procedure to parse out the Enneagram data for Preferred Conflict Processing Strategy
-- =============================================
CREATE PROCEDURE [dbo].[ParseConflictProcessingStrategy]
	-- Add the parameters for the stored procedure here
	@Name varchar(75) ,
	@StringVar varchar(5000)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
Declare @keyPhrase varchar(100) = 'Your preferred conflict processing strategy is'
Declare @keyPhraseLength int
Select @keyPhraseLength = LEN(@keyPhrase)

DECLARE @closingKeyPhrase varchar(50) = '.What'
SELECT SUBSTRING(@StringVar,CHARINDEX(@keyPhrase, @StringVar)+@keyPhraseLength,CHARINDEX(@closingKeyPhrase,@StringVar)-(CHARINDEX(@keyPhrase, @StringVar)+@keyPhraseLength))


END
GO
