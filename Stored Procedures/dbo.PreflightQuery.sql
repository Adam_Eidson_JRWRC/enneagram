SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Adam Eidson
-- Create date: 1/30/2019
-- Description:	compiles preflight data
-- =============================================
--exec Enneagram.dbo.[PreflightQuery] 
CREATE PROCEDURE [dbo].[PreflightQuery] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;with cte as 
(SELECT  pd.name
		,pd.title
		,pd.department
		,pd.company
		,[user_id]
      ,[ email_alias]
      ,[ user_email]
      ,[ first_name]
      ,[ last_name]
      ,[ company_code]
      ,[ question]
      ,[ answer]
	  ,pf.[ first_name]+' '+pf.[ last_name] as fullName
      ,r.[EnneagramType]
      ,r.[EnneagramTypeAKA]
      ,r.[GoalPresentation]
      ,r.[Instinct]
      ,r.[DominantCenter]
      ,r.[WeakestCenter]
      ,r.[LevelOfIntegration]
      ,r.[MetaMessage]
	  ,r.[OverallStrain]
      ,r.[EnvironmentalStrain]
      ,r.[VocationalStrain]
      ,r.[PhysicalStrain]
      ,r.[InterpersonalStrain]
      ,r.[PsychologicalStrain]
      ,r.[HappinessLevel]
	  ,r.[ConflictStrategy]
  FROM Enneagram.dbo.PersonnelDepartment pd
  left join [Enneagram].[dbo].[eval-account-question-answers] pf on pd.name = LTRIM(RTRIM(pf.[ first_name]))+' '+LTRIM(RTRIM(pf.[ last_name]))
  left join Enneagram.dbo.results r on LTRIM(RTRIM(r.name)) = LTRIM(RTRIM(pd.name))
  where LTRIM(RTRIM(r.name)) in ('Bradley Fisher',
'Brenna Gelbaum',
'Christabele Butler',
'Christie Burrow',
'Dan Werry',
'Dave Van Steenis',
'David Fisher',
'David Gunnel',
'Jon Taylor',
'Joshua Ungerecht',
'Kevin Steines',
'Kevin Zwick',
'Louis Swingrover',
'Matias Ichaso',
'Meryl Maglaya',
'Nicole Young',
'Pateel Tavidian',
'Rebecca Carney',
'Rena Morris',
'Riley Blindt',
'Robert Fox',
'Susana Dryden',
'Telma Altoon',
'Warren Thomas',
'Weldon Evans')
 )
 select * 
 from cte c
 PIVOT(
 max([ answer])
 FOR [ question] IN
 ([ What is your relationship to RISK (in general)?],
 [ What is your relationship to CHANGE?],
 [ How many VARIABLES can you have swimming around in your thinking before you go on tilt?],
 [ How do you think LOGICALLY?],
[ What is your relationship to AMBIGUITY?],
[ What is your relationship to OPPORTUNITY?]
)
 ) as pivotTable
 

END
GO
